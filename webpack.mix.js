const { mix } = require('laravel-mix');

// Mobile
mix.js('src/resources/assets/js/app.js', 'public/js')
    .sass('src/resources/assets/sass/app.scss', 'public/css');

// Mobile
mix.combine([
    'node_modules/framework7/dist/css/framework7.ios.min.css',
    'node_modules/framework7/dist/css/framework7.ios.colors.min.css',
    'node_modules/framework7-icons/css/framework7-icons.css'
], 'public/css/f7.css', 'node_modules')
    .copy('node_modules/framework7-icons/fonts', 'public/fonts');

// Global
mix.combine([
    'node_modules/font-awesome/css/font-awesome.min.css'
], 'public/css/app-dependencies.css', 'node_modules')
    .copy('node_modules/font-awesome/fonts', 'public/fonts');